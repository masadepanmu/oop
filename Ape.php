<?php

require_once('animal.php');

class Ape extends Animal {
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    public function __construct($nama){
        $this->name = $nama;
    }
    public function yell(){
        echo "Auooo";
    }
    
}

?>