<?php

require_once('animal.php');

class Frog extends Animal {
    public $name;
    public $legs = 2;
    public $cold_blooded = "no";

    public function __construct($nama){
        $this->name = $nama;
    }
    public function jump(){
        echo "Hop Hop";
    }
    
}

?>